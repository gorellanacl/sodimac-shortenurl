/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.sodimac.shortenurls;

import cl.sodimac.shortenurls.controllers.ShortenUrlStore;

/**
 *
 * @author gasto
 */
public class ShortenUrlApp {
    public static void main(String[] args) {
        
        ShortenUrlStore store = new ShortenUrlStore();
        
        // Shorting
        String shortStringUrl = store.shortStringUrl("http://www.google.cl/holamundo");
        String shortStringUrl2 = store.shortStringUrl("http://www.google.cl/otrotest");
        String shortStringUrl3 = store.shortStringUrl("http://www.soyunaurlmuylarga/yopartedeluri?key=value");
        
        System.out.println(shortStringUrl);
        System.out.println(shortStringUrl2);
        System.out.println(shortStringUrl3);
        
        // Recovering initial values
        String initialUrl = store.initialUrl(shortStringUrl);
        String initialUrl2 = store.initialUrl(shortStringUrl2);
        String initialUrl3 = store.initialUrl(shortStringUrl3);
        
        System.out.println(initialUrl);
        System.out.println(initialUrl2);
        System.out.println(initialUrl3);
        
        // Using wrong value
        String initialUrlX = store.initialUrl(shortStringUrl+"X");
        System.out.println(initialUrlX);
        
    }
}
