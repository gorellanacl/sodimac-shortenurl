/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.sodimac.shortenurls.dto;

/**
 *
 * @author gasto
 */
public class Conversion {
    
    private String originalValue;
    private String convertedValue;

    public Conversion() { }
    public Conversion(String originalValue, String convertedValue) {
        this.originalValue = originalValue;
        this.convertedValue = convertedValue;
    }

    public String getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }

    public String getConvertedValue() {
        return convertedValue;
    }

    public void setConvertedValue(String convertedValue) {
        this.convertedValue = convertedValue;
    }
    
}
