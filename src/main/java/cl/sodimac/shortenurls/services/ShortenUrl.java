/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.sodimac.shortenurls.services;

import java.util.ArrayList;

/**
 * Logic for conversion from decimal base (10) to base62 and reverse
 * 
 * @author gasto
 */
public class ShortenUrl {
    
    final static char[] LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    // Base 62 because LIST has 62 elements
    final static int BASE62 = 62;
    
    /**
     * Decimal value to Base62 for returning related value as string
     * 
     * @param id    Decimal value to be converted to Base62
     * @return String
     */
    public String idToBase62(int id) {
        ArrayList<String> rest = new ArrayList<>(); 
        while(id > 0) {
            char c = LIST[id%BASE62];
            rest.add(String.valueOf(c));
            id /= BASE62; 
        }
        
        String str = String.join("", rest);
        StringBuilder sb = new StringBuilder(str);
        
        return sb.reverse().toString();
    }
    
    /**
     * Base62 value to decimal value
     * 
     * @param base62    Base62 value to convert to decimal
     * @param l         Base62 value's length
     * @return 
     */
    public int base62ToId(String base62, int l) {
        
        int sum = 0;
        int exp = l-1;
        
        for(int i=0; i<l; i++) {
            // index found is equal to decimal value
            int index = this._getIndex(base62.charAt(i));
            sum += (index * Math.pow(BASE62, exp));
            exp--;
        }
        
        return sum;
    }
    
    /**
     * Simple index finder
     * 
     * @param value Value's index to find
     * @return int
     */
    private int _getIndex(char value) {
        int l = LIST.length;
        for(int i=0; i<l;i++) { if(LIST[i]==value) return i; }
        return -1;
    }
}
