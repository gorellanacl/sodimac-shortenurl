/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.sodimac.shortenurls.controllers;

import java.util.HashMap;
import cl.sodimac.shortenurls.dto.Conversion;
import cl.sodimac.shortenurls.services.ShortenUrl;

/**
 *
 * @author gasto
 */
public class ShortenUrlStore {
    
    // Store all conversions done
    HashMap<Integer, Conversion> db;
    // Abstraction to do tasks related to shorting urls
    ShortenUrl service;
    
    public ShortenUrlStore() {
        db = new HashMap<>();
        service = new ShortenUrl();
    }
    
    /**
     * Store conversion and return value proccesed
     * 
     * @param url   Original value to be shorten
     * @return String
     */
    public String shortStringUrl(String url) {
        
        // Create object to be stored
        Conversion conversion = new Conversion();
        conversion.setOriginalValue(url);
        
        // Get next index, convert and then return it
        int index = db.size() + 1;
        String s = service.idToBase62(index);
        
        conversion.setConvertedValue(s);
        db.put(index, conversion);
        
        return s;
    }
    
    /**
     * Get original value from shorten value
     * 
     * @param shortenValue    Shorten url
     * @return String
     */
    public String initialUrl(String shortenValue) {
        
        // Calculate id to find it into store
        int l = shortenValue.trim().length();
        if(l == 0) { return "Empty string detected"; }
        
        int index = service.base62ToId(shortenValue, l);
        if(!db.containsKey(index)) { return "Any URL has been found"; }
        
        return db.get(index).getOriginalValue();
    }
    
}
