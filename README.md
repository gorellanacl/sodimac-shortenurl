﻿# Shorten URL

Aplicación para generar URL acotadas. La estrategia utilizada fue jugar con el tema de la conversión de bases respecto a un índice único.

---

### Requerimientos

1. `Java v1.8`

### Tecnologías utilizadas

1. `Java`
2. `Maven` para el control / gestión de dependencias 

### Puesta en marcha
1. Clonar desde el repositorio
``` git
git clone https://gorellanacl@bitbucket.org/gorellanacl/sodimac-shortenurl.git
```

2. Abrir la aplicación con cualquier IDE y __ejecutarla__. En el método main se dejaron unos System.out.println("") con las pruebas realizadas.